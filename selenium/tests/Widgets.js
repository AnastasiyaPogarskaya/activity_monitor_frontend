import Driver from '../Driver'
import Widgets from '../lib/Widgets'

Promise.USE_PROMISE_MANAGER = false

const timeout = 50000

describe('Check avalability', function () {
	this.timeout(timeout)
	let driver, widgets

	before(async () => {
		driver = Driver.startDriver()
		driver.manage().window().maximize()
		widgets = new Widgets(driver)
	})

	it('Visit page', async () => {
		await widgets.visit()
	})

	it('Check cores', async () => {
		await widgets.checkCore()
	})

	it('Check CPU charts', async () => {
		await widgets.checkCpuChart()
	})

	it('Check RAM chart', async () => {
		await widgets.checkRamChart()
	})

	it('Check log list', async () => {
		await widgets.checkLog()
	})

	after(async () => {
		await driver.quit()
	})
})
