import Driver from '../Driver'
import AddNewDataWidget from '../lib/AddNewDataWidget'

Promise.USE_PROMISE_MANAGER = false

const timeout = 50000
const VALUE_CPU = '99'

describe('Check CPU data adding', function () {
	this.timeout(timeout)
	let driver, addNewDataWidget

	before(async () => {
		driver = Driver.startDriver()
		driver.manage().window().maximize()
		addNewDataWidget = new AddNewDataWidget(driver)
	})

	it('Visit page', async () => {
		await addNewDataWidget.visit()
	})

	it('Check select', async () => {
		await addNewDataWidget.checkSelect()
	})

	it('Select CPU', async () => {
		await addNewDataWidget.selectCPU()
	})

	it('Set input data', async () => {
		await addNewDataWidget.setValue(VALUE_CPU)
	})

	it('Send data CPU', async () => {
		await addNewDataWidget.submitForm()
	})

	after(async () => {
		await driver.quit()
	})
})
