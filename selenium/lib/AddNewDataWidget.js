import { By, until } from 'selenium-webdriver'
import { URL } from './const'
import OftenUsed from './OftenUsed'

const timerWait = 40000

export default class AddNewDataWidget {
	constructor(driver) {
		this.driver = driver
		this.url = URL
		this.selectWidgetLocator = By.id('adding_parameters_widget')
		this.inputNameLocator = By.id('adding_parameters_value')
		this.submitLocator = By.id('submit-form')
		this.selectDropdownLocator = By.className('ant-select-dropdown')
		this.selectItemLocator = By.className('ant-select-item-option-content')
	}

	async visit() {
		await this.driver.get(this.url)
		const currentUrl = await this.driver.getCurrentUrl()
		return new Promise((resolve, reject) => {
			if (currentUrl === this.url) {
				resolve(true)
			} else {
				reject(new Error('url was not found'))
			}
		})
	}

	async checkSelect() {
		return await this.driver.wait(until.elementLocated(this.selectWidgetLocator), timerWait)
	}

	async selectCPU() {
		const select = await this.driver.findElement(this.selectWidgetLocator)
		await select.click()
		await this.driver.wait(until.elementLocated(this.selectDropdownLocator), timerWait)
		const selectDropdown = await this.driver.findElement(this.selectDropdownLocator)
		const itemsDropdown = await selectDropdown.findElements(this.selectItemLocator)
		await itemsDropdown[0].click()
	}

	async selectRAM() {
		const select = await this.driver.findElement(this.selectWidgetLocator)
		await select.click()
		await this.driver.wait(until.elementLocated(this.selectDropdownLocator), timerWait)
		const selectDropdown = await this.driver.findElement(this.selectDropdownLocator)
		const itemsDropdown = await selectDropdown.findElements(this.selectItemLocator)
		await itemsDropdown[1].click()
	}

	async setValue(val) {
		const inputName = await this.driver.findElement(this.inputNameLocator)
		return await OftenUsed.characterByCharacter(this.driver, inputName, val)
	}

	async submitForm() {
		const button = await this.driver.findElement(this.submitLocator)
		await button.click()
	}
}
