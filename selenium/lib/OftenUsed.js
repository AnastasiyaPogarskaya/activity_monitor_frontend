import { By, until, Key } from 'selenium-webdriver'

const timerWait = 40000

export default class OftenUsed {
	//Entering characters one by one
	static async characterByCharacter(driver, webElem, string) {
		const arrayStr = string.split('')
		let str = ''
		for (let i = 0; i < arrayStr.length; i++) {
			await webElem.sendKeys(arrayStr[i])
			str += arrayStr[i]
			await driver.wait(async () => {
				const value = await webElem.getAttribute('value')
				if (value === str) {
					return value
				}
			}, 2000)
		}
		return Promise.resolve(true)
	}
}
