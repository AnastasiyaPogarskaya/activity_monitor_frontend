import { By, until } from 'selenium-webdriver'
import { URL } from './const'

const timerWait = 40000

export default class Widgets {
	constructor(driver) {
		this.driver = driver
		this.url = URL
		this.cpuChartLocator = By.id('cpu-chart')
		this.ramChartLocator = By.id('ram-chart')
		this.logLocator = By.className('log-card')
		this.coreLocator = By.className('core-usage')
	}

	async visit() {
		await this.driver.get(this.url)
		const currentUrl = await this.driver.getCurrentUrl()
		return new Promise((resolve, reject) => {
			if (currentUrl === this.url) {
				resolve(true)
			} else {
				reject(new Error('url was not found'))
			}
		})
	}

	async checkCpuChart() {
		return await this.driver.wait(until.elementLocated(this.cpuChartLocator), timerWait)
	}

	async checkRamChart() {
		return await this.driver.wait(until.elementLocated(this.ramChartLocator), timerWait)
	}

	async checkCore() {
		return await this.driver.wait(until.elementLocated(this.coreLocator), timerWait)
	}

	async checkLog() {
		return await this.driver.wait(until.elementLocated(this.logLocator), timerWait)
	}
}
