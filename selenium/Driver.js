import chrome from 'selenium-webdriver/chrome'
import chromedriver from 'chromedriver'

export default class Driver {
	static startDriver() {
		const service = new chrome.ServiceBuilder(chromedriver.path).build(),
			options = new chrome.Options()
		return chrome.Driver.createSession(options, service)
	}
}
