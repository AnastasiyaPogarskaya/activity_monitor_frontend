import React, { useState, useEffect } from 'react'
import moment from 'moment'

const Clock = () => {
	const [time, setTime] = useState(new Date())

	useEffect(() => {
		const interval = setInterval(() => setTime(new Date()), 1000)
		return () => clearInterval(interval)
	}, [])

	return <div className="clock">{moment(time).utcOffset(180).format('Do MMMM YYYY, HH:mm:ss')}</div>
}

export default Clock
