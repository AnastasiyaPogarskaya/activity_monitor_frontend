import React from 'react'
import { Line } from 'react-chartjs-2'

import './LineGraph.css'

const LineGraph = ({ data, options, id }) => (
	<div id={id} className="line-chart-wrapper">
		<Line data={data} options={options} width={100} height={50} />
	</div>
)

export default LineGraph
