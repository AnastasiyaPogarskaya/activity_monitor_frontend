import React from 'react'
import { Card, Statistic } from 'antd'

import './ColorCard.css'

const ColorCard = ({ title, value, middleLimit, highLimit, suffix }) => {
	const valueShow = value ? value : 'N/A'

	const getClassName = () => {
		switch (true) {
			case value && value >= middleLimit && value < highLimit:
				return 'warning-background'
			case value && value >= highLimit:
				return 'danger-background'
			case value && value < middleLimit:
				return 'normal-background'
			default:
				return 'empty-background'
		}
	}

	return (
		<Card className={getClassName()}>
			<Statistic className="core-usage" title={title} value={valueShow} precision={2} suffix={value && suffix} />
		</Card>
	)
}

export default ColorCard
