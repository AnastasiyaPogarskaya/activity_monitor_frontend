import React from 'react'
import { Row, Col } from 'antd'
import Clock from '../Clock'

const TopHead = () => (
	<div className="header-wrapper">
		<Row type="flex" justify="space-between" gutter={[16, 16]}>
			<Col>PC Activity Monitor</Col>
			<Col>
				<Clock />
			</Col>
		</Row>
	</div>
)

export default TopHead
