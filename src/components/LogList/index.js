import React from 'react'
import { Card, List, Typography } from 'antd'
import moment from 'moment'
import { observer } from 'mobx-react-lite'
import { useStore } from '../../stores'

import './LogList.css'

const LogList = observer(() => {
	const { activityMonitorStore } = useStore()
	const { logList } = activityMonitorStore

	return (
		<Card title="Log list" bordered={true} className="log-card">
			<List
				className="list-wrapper"
				bordered
				dataSource={logList}
				renderItem={(item) => (
					<List.Item>
						<Typography.Text mark>{`[ ${moment(item.date)
							.utcOffset(180)
							.format('DD.MM.YYYY HH:mm')} ]`}</Typography.Text>{' '}
						{item.log}
					</List.Item>
				)}
			/>
		</Card>
	)
})

export default LogList
