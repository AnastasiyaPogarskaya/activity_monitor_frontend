import React, { memo } from 'react'
import LineGraph from '../ui/LineGraph'
import moment from 'moment'
import { observer } from 'mobx-react-lite'
import { useStore } from '../../stores'

const CpuChart = observer(() => {
	const { activityMonitorStore } = useStore()
	const { cpuUsage } = activityMonitorStore

	const dd = cpuUsage.map((cu) => ({ x: moment(cu.date).add(-3, 'hour'), y: cu.usage }))

	const data = {
		labels: dd,
		datasets: [
			{
				label: '% of use CPU',
				fill: true,
				lineTension: 0.1,
				backgroundColor: 'rgba(75,192,192,0.4)',
				borderColor: 'rgba(75,192,192,1)',
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: 'rgba(75,192,192,1)',
				pointBackgroundColor: '#fff',
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: 'rgba(75,192,192,1)',
				pointHoverBorderColor: 'rgba(220,220,220,1)',
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: dd,
			},
		],
	}

	const options = {
		scales: {
			xAxes: [
				{
					type: 'time',
					distribution: 'series',
				},
			],
		},
		maintainAspectRatio: false,
	}

	return <LineGraph id="cpu-chart" data={data} options={options} />
})

export default memo(CpuChart)
