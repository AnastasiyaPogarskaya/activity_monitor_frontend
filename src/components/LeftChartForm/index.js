import React from 'react'
import FormParameters from './FormParameters'
import { observer } from 'mobx-react-lite'
import { useStore } from '../../stores'

import { WIDGETS_TYPE } from './helper'

const LeftChartForm = observer(() => {
	const { activityMonitorStore } = useStore()
	const { updateCpuUsage, updateRamUsage } = activityMonitorStore

	const onSubmit = async (values) => {
		const { widget, value } = values

		switch (widget) {
			case WIDGETS_TYPE.CPU:
				await updateCpuUsage(value)
				return
			case WIDGETS_TYPE.RAM:
				await updateRamUsage(value)
				return
			default:
				return
		}
	}

	return <FormParameters onSubmit={onSubmit} />
})

export default LeftChartForm
