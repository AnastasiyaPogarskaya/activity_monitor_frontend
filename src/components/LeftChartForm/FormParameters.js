import React from 'react'
import { Form, Input, Select, Button } from 'antd'

import './FormParameters.css'

import { WIDGETS } from './helper'

const FormParameters = ({ onSubmit }) => {
	return (
		<Form name="adding_parameters" layout="vertical" onFinish={(value) => onSubmit(value)}>
			<Form.Item name="widget" label="Widget">
				<Select>
					{WIDGETS.map((w) => (
						<Select.Option key={w.id} value={w.id}>
							{w.name}
						</Select.Option>
					))}
				</Select>
			</Form.Item>

			<Form.Item
				name="value"
				label="Value"
				rules={[
					{
						required: true,
						message: 'Value is required!',
					},
				]}
			>
				<Input type="number" min={0} max={100} />
			</Form.Item>

			<Form.Item>
				<Button type="primary" htmlType="submit" id="submit-form">
					Submit
				</Button>
			</Form.Item>
		</Form>
	)
}

export default FormParameters
