export const WIDGETS = [
	{ id: 'CPU', name: 'CPU usage' },
	{ id: 'RAM', name: 'RAM usage' },
]

export const WIDGETS_TYPE = {
	CPU: 'CPU',
	RAM: 'RAM',
}
