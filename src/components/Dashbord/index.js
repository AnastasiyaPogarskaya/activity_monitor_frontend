import React, { useEffect } from 'react'
import { Col, Row, Spin } from 'antd'
import { observer } from 'mobx-react-lite'

import ColorCard from '../ui/ColorCard'
import CpuChart from '../CpuChart'
import RamChart from '../RamChart'
import LogList from '../LogList'

import { useStore } from '../../stores/index'

const Dashboard = observer(() => {
	const { activityMonitorStore } = useStore()
	const { cpuCores, isLoadingCores, startTimeAndLoadData, stopTimer } = activityMonitorStore

	useEffect(() => {
		startTimeAndLoadData()
		return () => stopTimer()
	}, [])

	return (
		<>
			<Spin spinning={isLoadingCores}>
				<Row type="flex" gutter={[8, 8]} justify="space-around" align="middle">
					{cpuCores.map((gc) => (
						<Col span={3} key={gc.cpu_core}>
							<ColorCard
								title={gc.cpu_core}
								value={gc.usage}
								middleLimit={50}
								highLimit={90}
								suffix="%"
							/>
						</Col>
					))}
				</Row>
			</Spin>
			<Row type="flex" gutter={[16, 16]} justify="center" align="middle">
				<Col span={22}>
					<CpuChart />
				</Col>
			</Row>
			<Row type="flex" gutter={[16, 16]} justify="center" align="middle">
				<Col span={22}>
					<RamChart />
				</Col>
			</Row>
			<Row type="flex" gutter={[16, 16]} justify="center" align="middle">
				<Col span={22}>
					<LogList />
				</Col>
			</Row>
		</>
	)
})

export default Dashboard
