import React, { memo } from 'react'
import LineGraph from '../ui/LineGraph'
import moment from 'moment'
import { observer } from 'mobx-react-lite'
import { useStore } from '../../stores'

const RamChart = observer(() => {
	const { activityMonitorStore } = useStore()
	const { ramUsage } = activityMonitorStore

	const dd = ramUsage.map((cu) => ({ x: moment(cu.date).add(-3, 'hour'), y: cu.usage }))

	const data = {
		labels: dd,
		datasets: [
			{
				label: '% of use RAM',
				fill: true,
				lineTension: 0.1,
				backgroundColor: 'rgba(238, 130, 238, 0.4)',
				borderColor: 'rgba(238, 130, 238)',
				borderCapStyle: 'butt',
				borderDash: [],
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointBorderColor: 'rgba(238, 130, 238)',
				pointBackgroundColor: '#fff',
				pointBorderWidth: 1,
				pointHoverRadius: 5,
				pointHoverBackgroundColor: 'rgba(238, 130, 238)',
				pointHoverBorderColor: 'rgba(238, 130, 238)',
				pointHoverBorderWidth: 2,
				pointRadius: 1,
				pointHitRadius: 10,
				data: dd,
			},
		],
	}

	const options = {
		scales: {
			xAxes: [
				{
					type: 'time',
					distribution: 'series',
				},
			],
		},
		maintainAspectRatio: false,
	}

	return <LineGraph id="ram-chart" data={data} options={options} />
})

export default memo(RamChart)
