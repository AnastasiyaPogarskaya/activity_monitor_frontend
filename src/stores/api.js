import axios from 'axios'

const API_URL = 'http://localhost:5000'

const axiosInstance = axios.create({
	baseURL: API_URL,
	headers: {
		'Content-Type': 'application/json',
	},
	mode: 'cors',
	credentials: 'include',
	withCredentials: true,
	timeout: 120000,
})

const request = {
	get: (url) =>
		axiosInstance
			.get(url)
			.then((response) => response.data)
			.catch(),
	delete: (url) =>
		axiosInstance
			.delete(url)
			.then((response) => response.data)
			.catch(),
	post: (url, body) =>
		axiosInstance
			.post(url, body)
			.then((response) => response.data)
			.catch(),
	put: (url, body) =>
		axiosInstance
			.put(url, body)
			.then((response) => response.data)
			.catch(),
}

const Cpu = {
	cores: () => request.get(`/api/v1/cpu/cores`),
	usage: () => request.get(`/api/v1/cpu/usage`),
	addCore: (body) => request.post(`/api/v1/cpu/cores`, body),
	updateCpuUsage: (body) => request.put(`/api/v1/cpu/usage`, body),
}

const Log = {
	list: () => request.get(`/api/v1/log`),
}

const Ram = {
	usage: () => request.get(`/api/v1/ram/usage`),
	updateRamUsage: (body) => request.put(`/api/v1/cpu/usage`, body),
}

export default {
	Cpu,
	Log,
	Ram,
}
