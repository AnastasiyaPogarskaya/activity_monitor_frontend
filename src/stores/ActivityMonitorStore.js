import { action, observable, runInAction } from 'mobx'
import { message } from 'antd'
import moment from 'moment'

import api from './api'

class ActivityMonitorStore {
	@observable cpuCores = [
		{
			cpu_core: 'CPU1',
			usage: null,
		},
	]

	@observable logList = []
	@observable cpuUsage = []
	@observable ramUsage = []

	@observable isLoadingCores = false
	@observable isLoadingUsage = false
	@observable isLoadingRam = false
	@observable isLoadingLogs = false

	@observable timer = 0
	@observable lastMinutes = 0

	@action
	startTimeAndLoadData = async () => {
		await this.loadAllWidget()
		this.lastMinutes = moment().minutes()
		this.timer = setInterval(() => {
			const nowMinutes = moment().minutes()
			if (this.lastMinutes < nowMinutes) {
				this.loadAllWidget()
			}
			this.lastMinutes = nowMinutes
		}, 1000) 
	}

	@action
	stopTimer = () => {
		clearInterval(this.timer)
	}

	@action loadAllWidget = async () => {
		return await Promise.all([this.fetchCpuUsage(), this.fetchCpuCores(), this.fetchRamUsage(), this.fetchLogs()])
	}

	@action fetchCpuCores = async () => {
		this.isLoadingCores = true
		try {
			const response = await api.Cpu.cores()
			runInAction(() => {
				this.cpuCores = response.data
			})
		} catch (e) {
			message.error('CPU core data was not loaded')
		} finally {
			runInAction(() => {
				this.isLoadingCores = false
			})
		}
	}

	@action fetchLogs = async () => {
		this.isLoadingLogs = true
		try {
			const response = await api.Log.list()
			runInAction(() => {
				this.logList = response.data
			})
		} catch (e) {
			message.error('Logs data was not loaded')
		} finally {
			runInAction(() => {
				this.isLoadingLogs = false
			})
		}
	}

	@action addCpuCore = async () => {
		this.isLoadingCores = true
		const newCore = { cpu_core: 'Core 8', workload: 20 }
		try {
			await api.Cpu.addCore(newCore)
			runInAction(() => {
				this.cpuCores = [...this.cpuCores, newCore]
			})
		} catch (e) {
			message.error('CPU core data was not loaded')
		} finally {
			runInAction(() => {
				this.isLoadingCores = false
			})
		}
	}

	@action fetchCpuUsage = async () => {
		this.isLoadingUsage = true
		try {
			const response = await api.Cpu.usage()
			runInAction(() => {
				this.cpuUsage = response.data.sort((a, b) => moment(a.date) - moment(b.date))
			})
		} catch (e) {
			message.error('CPU core data was not loaded')
		} finally {
			runInAction(() => {
				this.isLoadingUsage = false
			})
		}
	}

	@action updateCpuUsage = async (value) => {
		this.isLoadingCores = true
		const date = moment().seconds(0).milliseconds(0).format()

		const newUsage = { date, usage: Number(value) }

		try {
			const response = await api.Cpu.updateCpuUsage(newUsage)
			runInAction(() => {
				this.cpuUsage = this.cpuUsage.map((u) =>
					u.id === response.data.id ? { ...u, usage: response.data.usage } : u,
				)
			})
			message.success(`CPU usage was updated`)

		} catch (e) {
			message.error('CPU core data was not loaded')
		} finally {
			runInAction(() => {
				this.isLoadingCores = false
			})
		}
	}

	@action fetchRamUsage = async () => {
		this.isLoadingRam = true
		try {
			const response = await api.Ram.usage()
			runInAction(() => {
				this.ramUsage = response.data.sort((a, b) => moment(a.date) - moment(b.date))
			})
		} catch (e) {
			message.error('RAM data was not loaded')
		} finally {
			runInAction(() => {
				this.isLoadingRam = false
			})
		}
	}

	@action updateRamUsage = async (value) => {
		this.isLoadingCores = true
		const date = moment().seconds(0).milliseconds(0).format()

		const newUsage = { date, usage: Number(value) }

		try {
			const response = await api.Ram.updateRamUsage(newUsage)
			runInAction(() => {
				this.ramUsage = this.ramUsage.map((u) =>
					u.id === response.data.id ? { ...u, usage: response.data.usage } : u,
				)
			})
			message.success(`RAM usage was updated`)
		} catch (e) {
			message.error('RAM data was not loaded')
		} finally {
			runInAction(() => {
				this.isLoadingCores = false
			})
		}
	}
}

export default new ActivityMonitorStore()
