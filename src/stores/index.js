import { createContext, useContext } from 'react'
import activityMonitorStore from './ActivityMonitorStore'

const stores = {
	activityMonitorStore,
}

export const StoreContext = createContext(stores)
export const useStore = () => {
	return useContext(StoreContext)
}
