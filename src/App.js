import React from 'react'
import { Layout } from 'antd'

import TopHead from './components/TopHead'
import LeftChartForm from './components/LeftChartForm'
import Dashboard from './components/Dashbord'

import './App.css'
import 'antd/dist/antd.css'

const { Header, Footer, Sider, Content } = Layout

function App() {
	return (
		<Layout className="wrapper">
			<Header className="header-wrapper">
				<TopHead />
			</Header>
			<Layout>
				<Sider className="left-side-wrapper">
					<LeftChartForm />
				</Sider>
				<Content className="content-wrapper">
					<Dashboard />
				</Content>
			</Layout>
			<Footer className="footer-wrapper">{`Matyushkina Anastasya SMB-701`}</Footer>
		</Layout>
	)
}

export default App
